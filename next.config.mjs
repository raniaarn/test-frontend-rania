/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: [
      'fastly.picsum.photos',
      'assets.suitdev.com'
    ]
  }
};

export default nextConfig;
