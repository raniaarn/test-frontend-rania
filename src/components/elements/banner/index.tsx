import Image from 'next/image';
import React, { useEffect, useState } from 'react';

export const Banner: React.FC<BannerProps> = ({ imageUrl, text, heading }) => {
  const [scrollPosition, setScrollPosition] = useState(0);

  useEffect(() => {
    const handleScroll = () => {
      setScrollPosition(window.pageYOffset);
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);


  return (
    <div className="relative h-96 overflow-hidden">
      <div>
        <Image src={imageUrl} fill sizes='none' alt=''/>
      </div>
      <div className="absolute inset-0 flex flex-col gap-2 text-center items-center justify-center text-white"
        style={{ transform: `translateX(-${scrollPosition}px)` }}>
        <h2 className="text-3xl font-bold">{heading}</h2>
        <p className="text-md">{text}</p>
      </div>
      <div className="absolute bottom-0 left-0 w-full h-24 bg-white"
        style={{
          clipPath: 'polygon(0 100%, 100% 0%, 100% 100%, 0% 120%)',
        }}
      />
    </div>
  );
};
