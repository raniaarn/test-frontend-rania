import { formatDate } from '@/components/utils/dateFormatter';
import { time } from 'console';
import Image from 'next/image';
import React, { useEffect, useState } from 'react';

export const Card: React.FC<CardProps> = ({
  id,
  small_image,
  published_at,
  title
}) => {

  const dummyImage = "https://fastly.picsum.photos/id/48/5000/3333.jpg?hmac=y3_1VDNbhii0vM_FN6wxMlvK27vFefflbUSH06z98so"
  const [imageError, setImageError] = useState(false);

  const handleImageError = () => {
    if (!imageError) {
      setImageError(true);
    }
  };

  return (
    <div key={id} className='flex flex-col'>
      <div className='relative aspect-[3/2] rounded-t-lg overflow-hidden'>
        <Image
          onError={handleImageError}
          src={imageError ? dummyImage : small_image[0].url}
          fill
          sizes='none'
          alt={small_image[0].file_name}
          loading='lazy'
        />
      </div>
      <div className="h-32 p-4 shadow-lg rounded-b-lg bg-white border-gray-100 border-2">
        <p className='text-xs text-gray-400'>
          {formatDate(published_at)}
        </p>
        <p className='whitespace-normal w-full font-bold line-clamp-3'>
          {title}
        </p>
      </div>
    </div>
  );
};
