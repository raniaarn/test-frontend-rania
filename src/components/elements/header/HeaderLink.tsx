import Link from "next/link"

export const HeaderLink: React.FC<HeaderLinkProps> = ({
  label,
  url,
  currentLocation
}) => {
  return (
    <Link
      href={url}
      className="group text-sm flex flex-col gap-2 items-center text-white">
      {label}
      {currentLocation == url && (
        <hr
          className={`w-full bg-white border-2`}
        />
      )}
    </Link>

  )
}
