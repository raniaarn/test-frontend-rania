import { useEffect, useState } from "react"
import Image from 'next/image'
import Link from "next/link"
import { HeaderLink } from "./HeaderLink"
import { useRouter } from "next/router"
import { FaBars, FaX } from "react-icons/fa6";

export const Header = () => {
  const [isCollapsed, setIsCollapsed] = useState(true)
  const router = useRouter()
  const [prevScrollPos, setPrevScrollPos] = useState(0);
  const [transparent, setTransparent] = useState(true);
  const [scrollUp, setScrollUp] = useState(true);
  const [isTop, setIsTop] = useState(true);

  const handleNavCollapse = () => setIsCollapsed(!isCollapsed)

  useEffect(() => {
    const handleScroll = () => {
      const currentScrollPos = window.pageYOffset;
      const isScrolledDown = prevScrollPos < currentScrollPos;
      setScrollUp(prevScrollPos > currentScrollPos)
      setTransparent(isScrolledDown || currentScrollPos != 0);
      setIsTop(currentScrollPos === 0);
      setPrevScrollPos(currentScrollPos);
    };

    window.addEventListener('scroll', handleScroll);

    return () => window.removeEventListener('scroll', handleScroll);
  }, [prevScrollPos]);
  
  return (
    <nav className={`px-4 fixed top-0 z-50 w-full flex flex-col justify-center bg-[#FF6600] ${transparent && !scrollUp && 'hidden'} ${scrollUp && !isTop && 'opacity-60'} ${isTop && 'opacity-100'}`} >
      <div className="flex items-center w-full py-4 px-7 mx-auto md:mx-0 md:px-20 justify-between">
        <Link href={'/'} as={'/'} className='flex-row items-center gap-4 flex'>
          <Image
            src={'/suitmedia.png'}
            alt={'Raniaarn'}
            width={80}
            height={80}
          />
        </Link>

        <div className={`gap-6 hidden md:flex`}>
          <HeaderLink label="Work" url="/Work" currentLocation={router.pathname} />
          <HeaderLink label="About" url="/About" currentLocation={router.pathname} />
          <HeaderLink label="Services" url="/Services" currentLocation={router.pathname} />
          <HeaderLink label="Ideas" url="/" currentLocation={router.pathname} />
          <HeaderLink label="Careers" url="/Careers" currentLocation={router.pathname} />
          <HeaderLink label="Contact" url="/Contact" currentLocation={router.pathname} />
        </div>

        {!isCollapsed ? (
          <button className="flex md:hidden" onClick={handleNavCollapse}>
            <FaX size={16} className="text-white" />
          </button>
        ) : (
          <button className="flex md:hidden" onClick={handleNavCollapse}>
            <FaBars size={16} className="text-white" />
          </button>
        )}
      </div>

      {!isCollapsed && (
        <div className="flex flex-col gap-4 py-8 items-center md:hidden">
          <HeaderLink label="Work" url="/Work" currentLocation={router.pathname} />
          <HeaderLink label="About" url="/About" currentLocation={router.pathname} />
          <HeaderLink label="Services" url="/Services" currentLocation={router.pathname} />
          <HeaderLink label="Ideas" url="/" currentLocation={router.pathname} />
          <HeaderLink label="Careers" url="/Careers" currentLocation={router.pathname} />
          <HeaderLink label="Contact" url="/Contact" currentLocation={router.pathname} />
        </div>
      )}
    </nav>
  )
}