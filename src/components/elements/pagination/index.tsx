import React, { useState, useEffect, useRef } from 'react'
import { FaChevronLeft, FaChevronRight, FaAngleDoubleRight, FaAngleDoubleLeft } from 'react-icons/fa'


export const Pagination: React.FC<PaginationProps> = ({ currentPage, totalPages, onPageChange }) => {
  const goToPreviousPage = () => {
    if (currentPage > 1) {
      onPageChange(currentPage - 1)
    }
  }

  const goToNextPage = () => {
    if (currentPage < totalPages) {
      onPageChange(currentPage + 1)
    }
  }

  const goToFinalPage = () => {
    onPageChange(totalPages)
  }

  const goToFirstPage = () => {
    onPageChange(1)
  }

  const pageButton = (page: number) => (
    <button
      key={page}
      type='button'
      className={`min-h-[38px] min-w-[38px] flex justify-center items-center ${page === currentPage
        ? 'text-white bg-[#FF6600]'
        : 'text-black'
        } py-2 px-3 text-sm rounded-lg focus:outline-none`}
      onClick={() => onPageChange(page)}
    >
      {page}
    </button>
  )

  const renderPageButtons = () => {
    const buttons = []

    const groupStartPage = Math.floor((currentPage - 1) / 4) * 4 + 1; // Menentukan halaman awal grup

    for (let i = groupStartPage; i < Math.min(groupStartPage + 4, totalPages + 1); i++) {
      buttons.push(pageButton(i));
    }

    return buttons
  }

  return (
    <nav className='flex justify-center items-center gap-x-7'>
      <button
        type='button'
        className={`items-center text-xl rounded-lg ${currentPage === 1 ? 'text-gray-400' : 'text-black'
          }`}
        onClick={goToFirstPage}
        disabled={currentPage === 1}
        aria-label='Next'
      >
        <FaAngleDoubleLeft />
      </button>
      <button
        type='button'
        className={`justify-center items-center gap-x-2 text-lg ${currentPage === 1 ? 'text-gray-400' : 'text-black'
          }`}
        onClick={goToPreviousPage}
        disabled={currentPage === 1}
        aria-label='Previous'
      >
        <FaChevronLeft />
      </button>
      <div className='flex items-center gap-x-7'>{renderPageButtons()}</div>
      <button
        type='button'
        className={`justify-center items-center gap-x-2 text-lg rounded-lg ${currentPage === totalPages ? 'text-gray-400' : 'text-black'
          }`}
        onClick={goToNextPage}
        disabled={currentPage === totalPages}
        aria-label='Next'
      >
        <FaChevronRight />
      </button>
      <button
        type='button'
        className={`items-center text-xl rounded-lg ${currentPage === totalPages ? 'text-gray-400' : 'text-black'
          }`}
        onClick={goToFinalPage}
        disabled={currentPage === totalPages}
        aria-label='Next'
      >
        <FaAngleDoubleRight />
      </button>
    </nav>
  )
}
