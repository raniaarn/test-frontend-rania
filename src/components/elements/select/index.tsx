import React, { useState } from 'react';

export const SelectDropdown: React.FC<SelectProps> = ({ options, onSelect }) => {
  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const value = e.target.value;
    onSelect(value);
  };

  return (
    <select className='border-2 text-xs rounded-xl px-2' onChange={handleChange}>
      {options.map((option, index) => (
        <option key={index} value={option}>{option}</option>
      ))}
    </select>
  );
};
