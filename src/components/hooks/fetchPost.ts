import axios from 'axios'
import { useEffect, useState } from 'react'

export const fetchPosts = ({ pageNum, pageSize, append, sortBy }: FetchPostProps) => {
  const [data, setData] = useState<PostData[]>([])
  const [totalData, setTotalData] = useState<number>(1)
  const [startIndex, setStartIndex] = useState<number>(1)
  const [endIndex, setEndIndex] = useState<number>(10)
  const [totalPages, setTotalPages] = useState<number>(10)

  const fetchingData = async () => {
    const url: string = `${process.env.NEXT_PUBLIC_BASE_API_URL}?page[number]=${pageNum}&page[size]=${pageSize}&append[]=small_image&append[]=medium_image&sort=${sortBy}`
    try {
      const response = await axios.get(url)
      setData(response.data.data as PostData[])
      setTotalData(response.data.meta.total)
      setStartIndex((pageNum - 1) * pageSize + 1);
      setEndIndex(Math.min(pageNum * pageSize, totalData));
      setTotalPages(response.data.meta.last_page)
      console.log(response)
    } catch (err: any) {
      console.log("Error")
    }
  }

  useEffect(() => {
    fetchingData()
  }, [pageNum, pageSize, append, sortBy])

  return { data, totalData, startIndex, endIndex, totalPages }
}

