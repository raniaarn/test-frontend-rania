import { Card, Pagination, SelectDropdown } from '@/components/elements';
import { fetchPosts } from '@/components/hooks/fetchPost';
import axios from 'axios';
import React, { useEffect, useState } from 'react';

export const ListPost = () => {
  const [showPerPage, setShowPerPage] = useState<number>(10);
  const [sortBy, setSortBy] = useState<"published_at" | "-published_at">("-published_at");
  const [currentPage, setCurrentPage] = useState<number>(1);
  const sortByOptions = ['Newest', 'Oldest'];
  const showPerPageOptions = ['10', '20', '50']

  const { data, totalData, startIndex, endIndex, totalPages } = fetchPosts({
    pageNum: currentPage,
    pageSize: showPerPage,
    append: "small_image",
    sortBy: sortBy
  })

  const handlePageChange = (page: number) => {
    setCurrentPage(page)
  }

  const handlePageSizeSelect = (selected: number) => {
    setShowPerPage(selected)
  }

  const handleSortBy = (selected: string) => {
    if (selected === "Newest") {
      setSortBy("-published_at");
    }

    if (selected === "Oldest") {
      setSortBy("published_at");
    }
  }

  return (
    <div className='p-16 md:p-24  flex flex-col gap-24'>
      <div className='flex flex-row'>
        <p className='text-xs w-full'>Showing {startIndex}-{endIndex} from {totalData}</p>
        <div className='flex flex-row w-full justify-end items-center gap-2'>
          <div className='flex flex-row gap-2 '>
            <p className='text-xs'>Show per page</p>
            <SelectDropdown options={showPerPageOptions} onSelect={handlePageSizeSelect} />
          </div>
          <div className='flex flex-row gap-2'>
            <p className='text-xs'>Sort By</p>
            <SelectDropdown options={sortByOptions} onSelect={handleSortBy} />
          </div>
        </div>
      </div>
      <div className="grid gap-4 grid-cols-2 md:grid-cols-4">
        {data.map(post => (
          <Card
            id={post.id}
            title={post.title}
            small_image={post.small_image}
            published_at={post.published_at}
          />
        ))}
      </div>
      <Pagination currentPage={currentPage} onPageChange={handlePageChange} totalPages={totalPages}></Pagination>
    </div>
  );
};
