interface BannerProps {
  imageUrl: string
  heading: string
  text: string
};