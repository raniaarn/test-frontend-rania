interface CardProps {
  id: number
  small_image: { file_name: string; id: number; mime: string; url: string }[];
  title: string
  published_at: string
};