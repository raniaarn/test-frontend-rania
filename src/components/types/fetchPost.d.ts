interface FetchPostProps {
  pageNum: number
  pageSize: number
  append: "small_image" | "medium_image"
  sortBy: "published_at" | "-published_at"
}