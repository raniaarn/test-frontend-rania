interface HeaderLinkProps {
  label: string
  url: string
  currentLocation: string
};