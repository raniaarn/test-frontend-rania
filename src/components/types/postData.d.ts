interface PostData {
  id: number
  small_image: { file_name: string; id: number; mime: string; url: string }[];
  published_at: string
  title: string
}

