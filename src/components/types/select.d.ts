interface SelectProps {
  options: string[] | number[]
  onSelect: (selected: T) => void;
}