export function formatDate(timestamp: string): string {
  const date = new Date(timestamp)

  let day: number | string = date.getUTCDate()
  day = day < 10 ? '0' + day : day

  const monthNames = [
    "Januari", "Februari", "Maret", "April", "Mei", "Juni",
    "Juli", "Agustus", "September", "Oktober", "November", "Desember"
  ];
  const month = monthNames[date.getUTCMonth()]

  const year = date.getFullYear()

  return `${day} ${month} ${year}`
}
