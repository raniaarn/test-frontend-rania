import { Inter } from "next/font/google";
import { Banner, Header } from "@/components";
import { ListPost } from "@/components/modules/ListPost";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <main>
      <Header />
      <Banner heading="Ideas" text="Where all our great things begin" imageUrl="https://fastly.picsum.photos/id/60/1920/1200.jpg?hmac=fAMNjl4E_sG_WNUjdU39Kald5QAHQMh-_-TsIbbeDNI"/>
      <ListPost />
    </main>
  );
}
